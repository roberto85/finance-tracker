class UserStocksController < ApplicationController

  def create
    stock = Stock.check_db(params[:ticker])
    if stock.blank?   
      stock = Stock.new_lookup(params[:ticker])
      stock.save
    end      
    @user_stock = UserStock.create(user: current_user, stock: stock)
    flash[:success] = "Stock #{stock.name} was successfully added to your portfolio"
    redirect_to my_portfolio_path
  end

  def destroy
    stock = Stock.find(params[:id])
    UserStock.find_by(user: current_user, stock: stock).destroy
    flash[:success] = "#{stock.ticker} is sucessfully removed from your portfolio" 
    redirect_to my_portfolio_path
  end

  def update
    @tracked_stocks = current_user.stocks
    @tracked_stocks.each do |stock|
      st = Stock.price_lookup(stock.ticker)
      stock.update(last_price: st.last_price)
    end
    redirect_to my_portfolio_path
  end

end
