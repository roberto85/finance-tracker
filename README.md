# README

The finance tracker app would enable you to track all of your stocks and share them amongst friends. This current version is only the framework and will evolve along the way. 
Things to know about the current version:

* Ruby version 2.6.3
* Rails version 6.0.2
* Devise for authentication
* Bootstrap added

Layouts are styled via Bootstrap
